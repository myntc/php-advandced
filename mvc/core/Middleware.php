<?php

class Middleware
{
    const DEFAULT_URI = './User/Login';
    const URIS_PROTECTED = [
        'Product/Add',
    ];

    public static function protect()
    {
        if (!empty($_GET['url'])) {
            foreach (self::URIS_PROTECTED as $uri) {
                if (strpos($_GET['url'], $uri) !== false && !isset($_SESSION['user'])) {
                    header('Location: ' . UrlBuilder::getUrl(self::DEFAULT_URI), true, 303);
                }
            }
        }
    }
}
