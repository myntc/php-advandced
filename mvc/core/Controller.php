<?php

class Controller
{
    public function getModel($model)
    {
        if (file_exists("./mvc/models/$model.php")) {
            require_once "./mvc/models/$model.php";
            return new $model;
        }
        throw new Exception("Not found $model class.");
    }

    public function getView($view, $data = [])
    {
        require_once "./mvc/views/$view.php";
    }

    public static function redirect($uri, $statusCode = 303)
    {
        if (filter_var($uri, FILTER_VALIDATE_URL) === false) {
            $uri = UrlBuilder::getUrl($uri);
        }
        header('Location: ' . $uri, true, $statusCode);
    }

    public function addFlashNotify($message, $level)
    {
        $_SESSION['notify']['message'] = $message;
        $_SESSION['notify']['level'] = $level;
    }
}
