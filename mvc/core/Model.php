<?php

class Model
{
    const ATTRIBUTE_NAMES = [];
    protected $table;
    protected $database;
    protected $data;

    public function __construct()
    {
        $this->database = new Database;
    }

    public function getAll()
    {
        return $this->database->fetchAll($this->table);
    }

    public function getById($id)
    {
        return $this->database->fetchById($this->table, $id);
    }

    public function getData() {
        if (!$this->data) {
            foreach (static::ATTRIBUTE_NAMES as $attribute_name) {
                if (!isset($_POST[$attribute_name])) {
                    throw new Exception("Not found '$attribute_name' field.");
                }
                $this->data[$attribute_name] = trim($_POST[$attribute_name]);
            }
        }
        return $this->data;
    }

    public function save() {
        $this->database->insert($this->table, $this->getData());
    }
}
