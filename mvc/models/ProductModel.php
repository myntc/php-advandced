<?php

class ProductModel extends Model
{
    const ATTRIBUTE_NAMES = [
        'name',
        'price',
        'quantity',
        'publisher',
    ];

    protected $table = 'product';

    public function getAvailableProducts() {
        $sql = "SELECT * FROM {$this->table} WHERE {$this->table}.quantity > 0";
        return $this->database->fetchSql($sql);
    }

    public function validate() {
        $errors = [];

        try {
            $data = $this->getData();
        } catch (Exception $e) {
            $errors[] = $e->getMessage();
            return $errors;
        }

        if (empty($data['name'])) {
            $errors[] = 'Name is not empty.';
        } elseif (strlen($data['name']) > 200) {
            $errors[] = 'Name must not be longer than 200 characters.';
        }

        if (empty($data['price']) && $data['price'] != 0) {
            $errors[] = 'Price is not empty.';
        } elseif (filter_var($data['price'], FILTER_VALIDATE_FLOAT) === false) {
            $errors[] = 'Price is not a float number.';
        }

        if (empty($data['quantity']) && $data['quantity'] != 0) {
            $errors[] = 'Quantity is not empty.';
        } elseif (filter_var($data['quantity'], FILTER_VALIDATE_INT) === false) {
            $errors[] = 'Quantity is not a integer number.';
        }

        if (empty($data['publisher'])) {
            $errors[] = 'Publisher is not empty.';
        } elseif (strlen($data['publisher']) > 100) {
            $errors[] = 'Publisher must not be longer than 100 characters.';
        }

        return $errors;
    }
}
