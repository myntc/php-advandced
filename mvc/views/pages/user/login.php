<div class="container">
    <?php if(isset($_SESSION['notify'])): ?>
        <div class="alert alert-<?= $_SESSION['notify']['level'] ?> mt-2" role="alert">
            <?= $_SESSION['notify']['message']; unset($_SESSION['notify']) ?>
        </div>
    <?php endif; ?>

    <div class="row mt-4">
        <div class="col-lg-9">
            <div class="card shadow mb-2">
                <div class="card-header bg-success text-center">
                    <h4 class="text-white font-weight-bold mb-0">Login Form</h4>
                </div>

                <form class="form-horizontal" action="<?= UrlBuilder::getUrl('User/login') ?>" method="POST">
                    <div class="card-body">
                        <div class="control-group row mb-3">
                            <label class="control-label col-md-3" id="username" for="email">Username</label>
                            <div class="col-md-9">
                                <input type="text" name="username" placeholder="Enter Username..." value="">
                            </div>
                        </div>

                        <div class="control-group row mb-3">
                            <label class="control-label col-md-3" for="pass">Password</label>
                            <div class="col-md-9">
                                <input type="password" id="password" name="password" placeholder="Enter Password..." value="">
                            </div>
                        </div>
                    </div>

                    <div class="card-footer clearfix footer-form-login">
                        <a href="">Fogot Password?</a>
                        <input type="submit" name="btn_login" class="btn btn-primary btn-user float-right" value="Login">
                    </div>
                </form>

            </div>

        </div>
        <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

</div>