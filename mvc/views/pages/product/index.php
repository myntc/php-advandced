<div class="row">
    <?php foreach ($data['products'] as $product) : ?>
        <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
            <div class="card h-100">
                <a href="#"><img class="card-img-top" src="<?= UrlBuilder::getUrl('public/images/' . $product['image']) ?>" alt=""></a>
                <div class="card-body">
                    <h4 class="card-title">
                        <a href="<?= UrlBuilder::getUrl("Product/Detail/{$product['id']}") ?>"><?= $product['name'] ?></a>
                    </h4>
                    <h5>$<?= $product['price'] ?></h5>
                    <p class="card-text"><?= $product['publisher'] ?></p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>