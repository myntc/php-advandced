<div class="card bg-primary text-white mb-4">
    <div class="card-body">
        <h5 class="m-0 font-weight-bold">New Product</h5>
    </div>
</div>

<?php if(!empty($data['errors'])): ?>
    <div class="alert alert-danger" role="alert">
        <?php foreach ($data['errors'] as $error): ?>
        <?= $error ?><br>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<form action="<?= UrlBuilder::getUrl('Product/Add') ?>" method="post">
    <div class="form-group">
        <label class="font-weight-bold" for="name">Name</label>
        <input type="text" name="name" class="form-control" placeholder="Enter name" id="name">
    </div>
    <div class="form-group">
        <label class="font-weight-bold" for="price">Price</label>
        <input type="text" name="price" class="form-control" placeholder="Enter price" id="price">
    </div>
    <div class="form-group">
        <label class="font-weight-bold" for="quantity">Quantity</label>
        <input type="number" name="quantity" class="form-control" placeholder="Enter quantity" id="quantity" min="0">
    </div>
    <div class="form-group">
        <label class="font-weight-bold" for="publisher">Publisher</label>
        <input type="text" name="publisher" class="form-control" placeholder="Enter publisher" id="publisher">
    </div>
    <div class="clearfix">
        <button type="submit" name="btn_save_product" class="btn btn-success float-right">Save</button>
    </div>
</form>