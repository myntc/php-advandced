<?php

class User extends Controller
{
    public function Index()
    {
        $this->getView('1column',
            [
                'page' => 'user/index',
            ]);
    }

    public function Login()
    {
        if (isset($_POST['btn_login'])) {
            if ($_POST['username'] === 'my' && $_POST['password'] === '123456') {
                $_SESSION['user'] = $_POST['username'];
                $this->redirect('Product/Add');
            } else {
                $this->addFlashNotify('Please check your account and password.', 'danger');
            }
        }

        $this->getView('1column',
            [
                'page' => 'user/login',
            ]);
    }

    public function Logout() {
        unset($_SESSION['user']);
        $this->redirect('User/Login');
    }
}