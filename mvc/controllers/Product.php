<?php

class Product extends Controller
{
    public function Index()
    {
        $productModel = $this->getModel('ProductModel');
        $products = $productModel->getAvailableProducts();

        $this->getView('1column',
            [
                'page' => 'product/index',
                'products' => $products,
            ]);
    }

    public function Detail($id)
    {
        $productModel = $this->getModel('ProductModel');
        $product = $productModel->getById($id);

        $this->getView('1column',
            [
                'page' => 'product/detail',
                'product' => $product,
            ]);
    }

    public function Add()
    {
        $errors = [];

        if (isset($_POST['btn_save_product'])) {
            $productModel = $this->getModel('ProductModel');
            $errors = $productModel->validate();
            if (empty($errors)) {
                $productModel->save();
                $this->addFlashNotify('Please check your account and password.', 'danger');
                $this->redirect('Product/Index');
            }
        }

        $this->getView('1column',
            [
                'page' => 'product/add',
                'errors' => $errors,
            ]);
    }
}
