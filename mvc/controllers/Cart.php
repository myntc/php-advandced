<?php

class Cart extends Controller
{
    public function Index()
    {
        $this->getView('1column',
            [
                'page' => 'cart/index',
                'total' => $this->getTotal()
            ]);
    }

    public function AddProduct($productId) {
        $productModel = $this->getModel('ProductModel');
        $product = $productModel->getById($productId);

        if ($product['quantity'] > 0) {
            if (isset($_SESSION['cart'][$product['id']])) {
                if ($_SESSION['cart'][$product['id']]['quantity'] < $product['quantity']) {
                    $product['quantity'] = $_SESSION['cart'][$product['id']]['quantity'] + 1;
                    $this->addFlashNotify('Added to cart success.', 'success');
                } else {
                    $this->addFlashNotify('The quantity of products has reached its maximum.', 'danger');
                }
            } else {
                $product['quantity'] = 1;
                $this->addFlashNotify('Added to cart success.', 'success');
            }
            $_SESSION['cart'][$product['id']] = $product;
        } else {
            $this->addFlashNotify('The product is out of stock!', 'danger');
        }

        $this->redirect("Product/Detail/$productId");
    }

    protected function getTotal()
    {
        $total = 0;
        if (!empty($_SESSION['cart'])) {
            foreach ($_SESSION['cart'] as $product) {
                $total += $product['price'] * $product['quantity'];
            }
        }
        return $total;
    }
}
